"""
Allow manual control over animation using the mouse and keyboard.

Based on:
https://stackoverflow.com/questions/44985966/managing-dynamic-plotting-in-matplotlib-animation-module/44989063#44989063
"""


from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import Slider


class FuncAnimationKey(FuncAnimation):
    """
    Subclass of matplotlibs FuncAnimation to incorporate some mouse and
    keyboard functionality to play, pause, and move through the data.
    """

    def __init__(self, fig, func, frames, slider_position=None,
            init_func=None, fargs=None, save_count=None,
            interval=200, repeat=False, blit=False,
            cache_frame_data=True, **kwargs):
        """
        API similar to that of FuncAnimation

        =INPUT=
            fig - matplotlib.figure.Figure
                Figure object
            func - callable
                Function handle to be called each frame
            frames - list, tuple, or int
                Specifying the frames that are passed to func
            slider_position - list or tuple
                Of the form (left, bottom, width, height) to specify the
                position and dimensions of a slider bar in the figure.

            For the rest, see matplotlib.animation.FuncAnimation docs
        =NOTES=
            - Both the FuncAnimation and the manual controls are going to
            be calling input func.
            - You MUST assign the DataPlayer object creation to a variable,
            otherwise the graph will not function.
        """

        # Init the superclass
        self.ani = FuncAnimation.__init__(self,
            fig, func, 
            frames=self.frame_generator(),
            init_func=init_func, fargs=fargs, save_count=save_count,
            interval=interval, repeat=repeat, blit=blit,
            cache_frame_data=cache_frame_data, **kwargs)

        self.fig = fig
        self.func = func
        self.frames = frames
        self.fargs = fargs

        # Parse frames
        if type(frames) == int:
            self.i_frame = 0
            self.idx_frame = 0
            self.min_frame = 0
            self.max_frame = frames
        elif hasattr(frames, '__iter__'):
            self.i_frame = frames[0]
            self.idx_frame = 0
            self.min_frame = 0
            self.max_frame = len(frames) - 1
        else:
            # TODO: Throw an error
            pass

        # Play state
        self.is_running = True
        self.is_forward = True
        
        # Bind canvas callbacks
        self.fig.canvas.mpl_connect('key_press_event',
            self.canvas_key_callbacks)
        self.fig.canvas.mpl_connect('button_press_event',
            self.canvas_mouse_button_callbacks)
        self.fig.canvas.mpl_connect('scroll_event',
            self.canvas_mouse_scroll_callbacks)

        # Create slider, if any
        if slider_position is None:
            slider_axes = plt.axes((0, 0.98, 0, 0))
        else:
            slider_axes = plt.axes(slider_position)
        self.slider_handle = Slider(slider_axes, '',
            valmin=self.min_frame, valmax=self.max_frame,
            valinit=self.idx_frame, valstep=1)
        self.slider_handle.on_changed(self.slider_callback)
        return


    def frame_generator(self):
        """
        The slider update is disconnected here, because otherwise this
        creates an infinite loop.
        """
        while self.is_running:
            self.update_frame(self.is_forward)
            self.update_slider(with_callback=False)
            yield self.i_frame
        return


    def update_frame(self, is_forward=True):
        """
        Wraps when exceeding min or max frame
        """
        
        if type(self.frames) is int:
            # For int
            self.i_frame += (int(is_forward) - int(not is_forward))
            if self.i_frame < self.min_frame:
                self.i_frame = self.max_frame
            elif self.i_frame > self.max_frame:
                self.i_frame = self.min_frame
            self.idx_frame = self.i_frame
        else:
            # For list or tuple
            self.idx_frame += (int(is_forward) - int(not is_forward))
            if self.idx_frame < 0:
                self.frame_idx = len(self.frames) - 1
            elif self.idx_frame > len(self.frames) - 1:
                self.frame_idx = 0
            self.i_frame = self.frames[self.idx_frame]
        return


    def toggle_play(self):
        """
        =NOTES=
            The update slider is called with callback at stop to make the
            slider functional. Without this call, for some reason, the slider
            callback is not triggered. Maybe has something to do with redrawing
            the slider in func.
        """
        if not self.is_running:
            self.event_source.start()
        else:
            self.event_source.stop()
            self.update_slider(with_callback=True)
        self.is_running ^= True
        return
    

    def toggle_play_direction(self):
        self.is_forward ^= True
        return


    def manual_frame_step(self, is_forward):
        self.update_frame(is_forward)
        self.update_slider()
        return


    def manual_func_call(self):
        """
        Call the function being executed every frame.
        TODO: blitting?
        """
        self.func(self.i_frame, *self.fargs)
        return


    def update_slider(self, with_callback=True):
        """
        NOTE: This triggers on_changed of the slider!
        """
        if not with_callback:
            self.slider_handle.eventson = False
        else:
            self.slider_handle.eventson = True
        self.slider_handle.set_val(self.idx_frame)
        return


    def slider_callback(self, slider_value):
        self.idx_frame = int(slider_value)
        if type(self.frames) is int:
            self.i_frame = self.idx_frame
        else:
            self.i_frame = self.frames.index(self.idx_frame)
        self.manual_func_call()
        return


    def canvas_key_callbacks(self, event):
        # Adust play state
        if event.key.isspace():
            self.toggle_play()
        if event.key == '/':
            self.toggle_play_direction()

        # Manual frame backward or forward
        if event.key == ',':
            self.manual_frame_step(is_forward=False)
        if event.key == '.':
            self.manual_frame_step(is_forward=True)

        return


    def canvas_mouse_button_callbacks(self, event):
        """
        1, 2, 3 for left, middle, right
        """
        if event.button == 2:
            self.toggle_play()
        return


    def canvas_mouse_scroll_callbacks(self, event):
        if event.button == 'up':
            self.manual_frame_step(is_forward=True)
        elif event.button == 'down':
            self.manual_frame_step(is_forward=False)
        return
