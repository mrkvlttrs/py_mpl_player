
## DataPlayer
DataPlayer from key_animation is a subclass of matplotlib FuncAnimation.
It adds some manual functionality to the plot.
Current controls consist of:

## Keyboard
* space bar     toggle play / pause
* /             change play direction
* ,             move one frame backwards
* .             move one frame forwards

## Mouse
* wheel press   toggle play / pause
* scroll down   move one frame backwards
* scroll up     move one frame forwards

For more information, see matplotlib.animation.FuncAnimation documentation.


## PlayerMocap
PlayerMocap from player_np3d contains a class for easy mocap data plotting.
Currently only runs in interactive mode. Not sure why.
Possibly because the figure handle is part of some class.

Basic usage:

player = PlayerMocap(data, frame_axis=0, dimension_axis=2)
player.show()

