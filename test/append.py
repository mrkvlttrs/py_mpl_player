
import json
import numpy as np

from py_mpl_player.player_3d import Player3d


path = '/home/mark/Documents/surfdrive/data/2014_PertWalkingDefv2/ALLDATA/141031PP1/VZ/141031Trial101'
# path = '/home/mark/Documents/surfdrive/data/2014_PertWalkingDefv2/ALLDATA/141031PP1/VZ/Probe/MLL'

pg = json.load(open(''.join([path, '.json'])))
arrays = np.load(''.join([path, '.npz']))

marker = arrays['marker']
point = arrays['point']

marker = marker[:, :, [2, 0, 1, 3]]
point = point[:, :, [2, 0, 1]]

player = Player3d(point, frame_axis=0, dimension_axis=2)
# player.append_trace(point)

player.show()

