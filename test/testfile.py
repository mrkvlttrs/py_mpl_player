
from py_readc3d.reader import Reader
from py_readc3d.converter import Converter

from player_3d import Player3d

from matplotlib import pyplot as plt


full_path = "/home/mark/Documents/surfdrive/data/2014_PertWalkingDefv2/ALLDATA/141031PP1/VZ/141031Trial101.C3D" 

reader = Reader()
converter = Converter(reader)

reader.read(full_path, header_is_truth=True)
pg = converter.to_dict()
marker, analog = converter.to_numpy()


marker[:] = marker[:, :, [0, 2, 1, 3]]

# NOTE: .show() only works when explicitly running python in interactive mode
# Otherwise you need pyplot.show()
player = Player3d(marker, frame_axis=0, dimension_axis=2)
player.show()

