"""
Template data player for 3d numpy data
"""


from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from py_mpl_player.animation import FuncAnimationKey


class Player3d(object):

    def __init__(self, data, frame_axis=0, dimension_axis=1, sample_time=10):
        """
        =INPUT=
            data - ndarray
                Numpy array of shape (K, L, M)
            frame_axis - scalar, int
                Indicating the axis that contains the frame data
            dimension_axis - scalar, int
                Indicating the axis that contains the dimensions of the data.
                X, Y, Z data is assumed in the first 3 entries of this axis.
            sample_time - scalar, int
                Sample time in milliseconds. Limited by the speed at which
                your hardware can render a new plot frame.
        =NOTES=
            All arrays added with append_trace must have the same lenght on
            the frame_axis and dimension_axis among them.
        """

        self.frame_axis = frame_axis
        self.dimension_axis = dimension_axis
        self.selectors = self.create_selectors()

        # Create the figure
        self.fig = plt.figure()
        self.ax = plt.subplot(1, 1, 1, projection='3d')

        # Set axes
        self.ax.set_xlim3d([-1, 1])
        self.ax.set_ylim3d([-1, 1])
        self.ax.set_zlim3d([0, 2])
        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')
        self.ax.set_zlabel('Z')

        # Parameters to update function
        self.data_list = []
        self.traces = []
        self.append_trace(data)

        # Add (manual) animation functionality
        self.ani = FuncAnimationKey(
            self.fig, self.update_function,
            frames=data.shape[frame_axis],
            slider_position=(0.1, 0.05, 0.8, 0.02),
            fargs=(self.data_list, self.traces),
            interval=sample_time)

        return


    def create_selectors(self):
        selectors = []
        for i_dimension in range(0, 3):
            selector = [slice(None)] * 3
            selector[self.frame_axis] = 0
            selector[self.dimension_axis] = i_dimension
            selectors.append(selector)
        return selectors


    def update_function(self, i_frame, data_list, traces):
        """
        Update the data
        """
        for selector in self.selectors:
            selector[self.frame_axis] = i_frame
        for data, trace in zip(data_list, traces):
            trace.set_data_3d(
                data[tuple(self.selectors[0])],
                data[tuple(self.selectors[1])],
                data[tuple(self.selectors[2])])
        return traces

    
    def append_trace(self, data):
        """
        TODO: add options to change marker properties
        """
        self.data_list.append(data)
        if not len(self.traces):
            self.traces = self.ax.plot([0, 0], [0, 0], [0, 0],
                marker='o', linestyle='')
        else:
            self.traces.append(self.ax.plot([0, 0], [0, 0], [0, 0],
                marker='o', linestyle='')[0])
        return


    def show(self):
        """
        This only works when you run python in interactive mode.
        """
        self.fig.show()
        return
